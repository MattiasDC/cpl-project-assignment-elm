module HotKeysManager where

import Signal
import Signal.Extra
import Char
import Maybe
import List
import ItemFeed
import String
import Keyboard exposing ( KeyCode )

-- Module to handle keyboard signals, extra shortkeys can be added to the keyCodeMap.
-- Extra shortkeys besides the ones that should have been provided by the basis assignment:
--      Alt-H : Toggle hiding of done items
--      Alt-A : Toggle hiding of "add reminder" section

-- Represents when a action should fire, when the key is down or up
type Press = Down|Up

-- Map the many signals from the different shortkeys to a single signal. Since different signals can fire at the same time
-- and Signal.mergeMany prefers the leftmost signal, the Signal.Extra module is used. With this module it is possible to
-- give preference to signal which actually contain an action (not Nothing).
signal : Signal (Maybe ItemFeed.Action)
signal =
    Signal.Extra.mapMany
        (\l -> Maybe.withDefault Nothing 
                    <| List.head
                        <| List.filter (\e -> containsAction e) l)
            <| List.map (\(s, a, t) -> Signal.dropRepeats <| Signal.map2 (hotKeyMap a t) Keyboard.alt s) keyCodeMap

containsAction : Maybe ItemFeed.Action -> Bool
containsAction action =
    case action of
        Just _ -> True
        Nothing -> False

keyCodeMap : List (Signal Bool, ItemFeed.Action, Press)
keyCodeMap =
    [
        (Keyboard.isDown <| Char.toCode 'J', ItemFeed.SelectNext, Down),
        (Keyboard.isDown <| Char.toCode 'K', ItemFeed.SelectPrevious, Down),
        (Keyboard.isDown <| Char.toCode 'O', ItemFeed.ToggleTruncate, Down),
        (Keyboard.isDown <| Char.toCode 'P', ItemFeed.TogglePinned, Down),
        (Keyboard.isDown <| Char.toCode 'X', ItemFeed.ToggleDone, Down),
        (Keyboard.isDown <| Char.toCode 'S', ItemFeed.SortReverse, Down),
        (Keyboard.isDown <| Char.toCode 'H', ItemFeed.ToggleHideDone, Down),
        (Keyboard.isDown <| Char.toCode 'A', ItemFeed.ToggleAddReminder, Down),
        (Keyboard.isDown <| Char.toCode 'S', ItemFeed.SortNormal, Up)
    ]


-- Function which decides whether an action based on the signal from keyboard should fire
hotKeyMap : ItemFeed.Action -> Press -> Bool -> Bool -> Maybe ItemFeed.Action
hotKeyMap action t alt kdown =
        if alt && (kdown && t == Down || not kdown && t == Up) then
            Just action
        else
            Nothing