module ItemFeed where

import Signal
import Html exposing ( Html )
import Html.Events as E
import Html.Attributes exposing ( style, type', name, placeholder, value )
import Maybe
import List
import Time
import Date exposing ( Date, Month )
import String
import Item
import DateManager
import Static

--Module representing an itemfeed which contains items.
--This itemfeed contains the items and has the functionality to add items (reminders)

type alias Model =  { items : List ItemElem
                    , selectedIndex : Int
                    , sortFunction : (ItemElem -> ItemElem -> Order)
                    , bodyInputField : String
                    , dateInputField : String
                    , deadlineInputField : String
                    , currentDate : Date
                    , showDone : Bool
                    , showAddReminder : Bool
                    }
-- An itemfeed element has a unique id, to perform update operations based on this id,
-- and an item model, to perform these action upon.
type alias ItemElem = (Int, Item.Model)

init : Model
init = 
    { items = List.indexedMap (\i e -> (i, e)) <| List.append (Item.initReminders Static.reminders) (Item.initEmails Static.emails)
    , selectedIndex = 0
    , sortFunction = sortNormal
    , bodyInputField = ""
    , dateInputField = DateManager.dateToString DateManager.beginDate
    , deadlineInputField = DateManager.dateToString DateManager.beginDate
    , currentDate = DateManager.beginDate
    , showDone = True
    , showAddReminder = True
    }

type Action = SelectNext
            | SelectPrevious
            | Select Int
            | SortNormal
            | SortReverse
            | ToggleTruncate
            | TogglePinned
            | ToggleDone
            | ToggleHideDone
            | ToggleAddReminder
            | Add Item.Model
            | AddMultiple (List Item.Model)
            | Update Int Item.Action
            | UpdateBodyField String
            | UpdateDateField String
            | UpdateDeadlineField String
            | UpdateDateTime Date

update : Action -> Model -> Model
update action model =
    case action of
        SelectNext              -> update (Select (model.selectedIndex + 1)) model
        SelectPrevious          -> update (Select (model.selectedIndex - 1)) model
        -- Selects the given index to be the focused index.
        -- If the length of the items that should be shown is 0, the index is set to 0.
        -- This index is the index in sorted list
        Select index            -> let l = List.length (getSortedShowing model)
                                        in { model - selectedIndex | selectedIndex = if l > 0 then index %  l else 0}
        -- Sorting actions, these set the sortfunction to different functions
        SortNormal              -> { model - sortFunction | sortFunction = sortNormal }
        SortReverse             -> { model - sortFunction | sortFunction = sortReverse }
        -- Toggling actions, notice that the itemfeed does not know whether the toggling is done on emails or reminders.
        ToggleTruncate          -> case getFocusedItem model of
                                        Just (_,(id, _)) -> update (Update id Item.ToggleTruncate) model
                                        _ -> model
        TogglePinned            -> case getFocusedItem model of
                                        Just (_,(id, _)) -> update (Update id Item.TogglePinned) model
                                        _ -> model
        ToggleDone              -> case getFocusedItem model of
                                        Just (_,(id, _)) -> refocusSelectedIndex <| update (Update id Item.ToggleDone) model
                                        _ -> model
        ToggleHideDone          -> refocusSelectedIndex { model - showDone | showDone = not model.showDone }
        ToggleAddReminder       -> { model - showAddReminder | showAddReminder = not model.showAddReminder }
        -- Adds a new item to the model if it is not yet in the itemFeed
        Add item                ->  if not (containsItem model item) then
                                        { model - items | items = (List.length model.items, item)::model.items }
                                    else
                                        model
        AddMultiple items           -> List.foldr (\i -> update (Add i)) model items 
        -- Update the item with the given unique id (not index in list) with the given action
        Update id itemAction    ->  { model - items |
                                        items =
                                            List.map 
                                                (\(i, e) -> if i == id then
                                                                (i, Item.update itemAction e) 
                                                            else
                                                                (i, e)
                                                )
                                                model.items
                                    }
        -- Update field actions. These are executed when the input fields on the html page are changed.
        -- These actions update the values of the model.
        UpdateBodyField body        -> { model - bodyInputField | bodyInputField = body }
                                        -- If the deadline field is past the current value of the date field,
                                        -- the deadline field is updated to have the value of the date field.
                                        -- This is done because having a deadline past the input date is not useful.
                                        -- Nevertheless for testing purposes it is possible to adjust this on the html page.                
        UpdateDateField date        -> let newM = { model - dateInputField | dateInputField = date }
                                        in  if compare newM.deadlineInputField newM.dateInputField == LT then
                                                update (UpdateDeadlineField date) newM
                                            else
                                                newM
        UpdateDeadlineField date    -> { model - deadlineInputField | deadlineInputField = date }
                                        -- The date field is set to the current date if it has not yet been initialized to it.
        UpdateDateTime date         -> let newM = { model - currentDate | currentDate = date }
                                        in  if DateManager.equal (DateManager.fromString newM.dateInputField) (DateManager.beginDate) then
                                                update (UpdateDateField (DateManager.dateToString date)) newM
                                            else
                                                newM

-- Sort function: as in the given example, older items are on top, giving preference to pinned items.
sortNormal : ItemElem -> ItemElem -> Order
sortNormal (_, a) (_, b) =
    if Item.isPinned a then
        if Item.isPinned b then
            DateManager.compareDate (Item.getDate a) (Item.getDate b)
        else
            LT
    else
        if Item.isPinned b then
            GT
        else
            DateManager.compareDate (Item.getDate a) (Item.getDate b)

-- Sort function: Newest items are on top.
sortReverse : ItemElem -> ItemElem -> Order
sortReverse (_, a) (_, b) =
    case (DateManager.compareDate (Item.getDate a) (Item.getDate b)) of
        GT -> LT
        EQ -> EQ
        LT -> GT

-- List of all todo items, while filtering the snoozed items.
getTodoSorted : Model -> List ItemElem
getTodoSorted model =
    List.sortWith model.sortFunction <| List.filter (\(i, e) -> not (Item.isDone e) && not (Item.isSnoozed e model.currentDate)) model.items

-- List of all done items, while filtering the snoozed items.
getDoneSorted : Model -> List ItemElem
getDoneSorted model =
   List.sortWith model.sortFunction <| List.filter (\(i, e) -> Item.isDone e && not (Item.isSnoozed e model.currentDate)) model.items

-- A list of all items that should be shown on the page, in the correct order
getSortedShowing : Model -> List ItemElem
getSortedShowing model =
    filterNotShowingItems model.showDone model.currentDate <| List.append (getTodoSorted model) (getDoneSorted model)

-- If the done items should not be shown, filter them out.
filterNotShowingItems : Bool -> Date -> List ItemElem -> List ItemElem
filterNotShowingItems showDone currentDate list =
    if showDone then
        list
    else
        List.filter (\(i, e) -> not (Item.isDone e)) list

containsItem : Model -> Item.Model -> Bool
containsItem model item =
    if (List.length <| List.filter (\(i, e) -> Item.equal e item) model.items) > 0 then
        True
    else
        False
-- Get the focused item from the model.
getFocusedItem : Model -> Maybe (Int, ItemElem)
getFocusedItem model =
    List.head <| List.filter (\(index, (i, item)) -> index == model.selectedIndex) <| List.indexedMap (,) (getSortedShowing model)

-- Check whether the unique id of the given item corresponds to the focused item.
isFocusedItem : Model -> Int -> Bool
isFocusedItem model id =
    case getFocusedItem model of
        Just (_, (i,_)) -> id == i
        Nothing -> False

-- After hide operations like snooze, done, ... The select focus should be reinitialised.
refocusSelectedIndex : Model -> Model
refocusSelectedIndex model =
    update (Select (getIdAfterHide model)) model

getIdAfterHide : Model -> Int
getIdAfterHide model =
    if model.selectedIndex < List.length (getSortedShowing model) then
        model.selectedIndex
    else
        let l = List.length (getSortedShowing model) - 1
        in  if l == -1 then
                0
            else
                l

-- VIEW

viewItems : Signal.Address Action -> Model -> String -> List ItemElem -> Maybe Html
viewItems address model title list=
        if not <| List.isEmpty list then
            Just <| Html.div [] <|
                Html.h1 [getHeaderStyle title] [ Html.text title ]::
                List.map (\(id, e) -> Item.view (Signal.forwardTo address (Update id)) (getSingleItemStyle model id e) model.currentDate e) list
        else
            Nothing

viewAddReminders : Signal.Address Action -> Model -> String -> Maybe Html
viewAddReminders address model title =
    if model.showAddReminder then 
        Just <| Html.div [] <|
            [
                Html.h1 [getHeaderStyle title] [Html.text title]
                , Html.div []
                            [
                                Html.input [type' "text", name "body", placeholder "reminder description",
                                        E.on "input" E.targetValue (\str -> Signal.message address (UpdateBodyField str))] []
                                , Html.input [type' "date", name "created", value model.dateInputField,
                                                E.on "input" E.targetValue
                                                    (\str -> Signal.message address
                                                        (UpdateDateField str))]
                                            [Html.text model.dateInputField]
                                , Html.button   [E.onClick address
                                        (Add <| Item.initReminderWithDeadline
                                                    {body = model.bodyInputField, created = model.dateInputField}
                                                        Nothing)]
                                    [Html.text "Add reminder"]
                                , Html.br [] []
                                , Html.text "Optional deadline: "
                                , Html.input [type' "date", name "deadline", value model.deadlineInputField,
                                                E.on "input" E.targetValue
                                                    (\str -> Signal.message address
                                                        (UpdateDeadlineField str))]
                                            [Html.text model.deadlineInputField]
                                , Html.button   [E.onClick address
                                                    (Add <| Item.initReminderWithDeadline
                                                                {body = model.bodyInputField, created = model.dateInputField}
                                                                    (Just (DateManager.fromString model.deadlineInputField)))]
                                                [Html.text "Add reminder with deadline"]
                            ]

            ]
    else
        Nothing

extractHtml : Maybe Html -> Html
extractHtml m =
    Maybe.withDefault (Html.text "") m

view : Signal.Address Action -> Model -> Html
view address model =
        Html.div [getItemFeedStyle model]
            [
                extractHtml <| viewItems address model "Todo" (getTodoSorted model)
                , Html.br [] []
                , (if model.showDone then extractHtml <| viewItems address model "Done" (getDoneSorted model) else Html.text "")
                , Html.br [] []
                , extractHtml <| viewAddReminders address model "Add Reminder"
            ]

-- STYLING

getItemFeedStyle : Model -> Html.Attribute
getItemFeedStyle model =
    style [("width","50%"), ("margin", "auto")]

getHeaderStyle : String -> Html.Attribute
getHeaderStyle title =
    if title == "Todo" then
        style [("border-bottom", "thick"), ("border-bottom", "solid"), ("border-bottom-color", "Orange")]
    else if title == "Done" then
        style [("border-bottom", "thick"), ("border-bottom", "solid"), ("border-bottom-color", "Green")]
    else
        style [("border-bottom", "thick"), ("border-bottom", "solid"), ("border-bottom-color", "Blue")]

getSingleItemStyle : Model -> Int -> Item.Model-> Maybe (List (String, String))
getSingleItemStyle model id item =
    if isFocusedItem model id then
        Just [("border-left-width", "thick"), ("border-left-style", "double"), ("border-left-color", "Green")]
    else
        Nothing