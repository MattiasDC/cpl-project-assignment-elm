module Item where

import Signal
import Html exposing ( Html )
import Html.Attributes exposing ( style )
import Maybe
import Reminder
import Email
import String exposing ( concat )
import Date exposing ( Date, Month )
import DateManager
import Debug

-- Module representing an item in an itemfeed. This item can either be an email or a reminder.
-- This module is for abstraction purposes, this way an itemfeed should not know anything about
-- the representation of emails and reminders itself.

type Model = Email Email.Model|Reminder Reminder.Model

type alias InitEmailParameters = 
    { 
    from : String
    , to : String
    , title : String
    , body : String
    , date : String
    }

type alias InitReminderParameters = 
    { 
    body : String
    , created : String
    }

initReminders : List InitReminderParameters -> List Model
initReminders reminders =
    List.map initReminder reminders

initReminder : InitReminderParameters -> Model
initReminder params =
    initReminderWithDeadline params Nothing

initReminderWithDeadline : InitReminderParameters -> Maybe Date -> Model
initReminderWithDeadline params dl =
    Reminder <| Reminder.init  
        params.body
        (DateManager.fromString params.created)
        dl

initEmails : List InitEmailParameters -> List Model
initEmails emails =
    List.map initEmail emails

initEmail : InitEmailParameters -> Model
initEmail params = 
    Email <| Email.init
        params.from
        params.to
        params.title
        params.body
        (DateManager.fromString params.date)

equal : Model -> Model -> Bool
equal model1 model2 =
    case model1 of
        Email e1 -> case model2 of
                        Email e2    -> Email.equal e1 e2
                        Reminder _  -> False
        Reminder e1 -> case model2 of
                        Email _     -> False
                        Reminder e2 -> Reminder.equal e1 e2

isDone : Model -> Bool
isDone model =
    case model of
        Email e -> e.done
        Reminder r -> r.done

isPinned : Model -> Bool
isPinned model =
    case model of
        Email e -> e.pinned
        Reminder r -> r.pinned

isSnoozed : Model -> Date -> Bool
isSnoozed model currentDate =
    case model of
        Email e     ->  case e.snoozed of
                            Nothing ->  False
                            Just d  ->  if DateManager.compareDate d currentDate == GT then
                                            True
                                        else
                                            False
        Reminder r  -> case r.snoozed of
                            Nothing ->  False
                            Just d  ->  if DateManager.compareDate d currentDate == GT then
                                            True
                                        else
                                            False


getDate : Model -> Date
getDate model =
        case model of
        Email e -> e.date
        Reminder r -> r.created

type Action = UpdateReminder Reminder.Action
            | UpdateEmail Email.Action
            | ToggleTruncate
            | TogglePinned
            | ToggleDone


update : Action -> Model -> Model
update action model =
    case (action, model) of
        (UpdateReminder subAction, Reminder r)  -> Reminder <| Reminder.update subAction r
        (UpdateEmail subAction, Email e)        -> Email <| Email.update subAction e
        (ToggleTruncate, Email e)               -> Email <| Email.update Email.More e
        (TogglePinned, Reminder r)              -> Reminder <| Reminder.update Reminder.Pin r
        (TogglePinned, Email e)                 -> Email <| Email.update Email.Pin e
        (ToggleDone, Reminder r)                -> Reminder <| Reminder.update Reminder.Mark r
        (ToggleDone, Email e)                   -> Email <| Email.update Email.Mark e
        _                                       -> model

-- Styling for the item, based on whether it is pinned or done or not
getItemStyle : Model -> Maybe (List (String, String)) -> Html.Attribute
getItemStyle model styling =
    let pinAttribute = 
            if isPinned model then [("backgroundColor","WhiteSmoke")] else []
        doneAttribute =
            if isDone model then [("opacity", "0.5")] else []
    in
        style <| List.concat [pinAttribute,
                                doneAttribute,
                                [("border-bottom-width", "thick"),
                                ("border-bottom-style", "solid"),
                                ("border-bottom-color", "LightGray"),
                                ("padding", "5px 5px 10px")],
                                Maybe.withDefault [] styling
                             ]

view : Signal.Address Action-> Maybe (List (String, String)) -> Date -> Model -> Html
view address styling currentDate item =
    case item of
        Email email -> Html.p [getItemStyle item styling ] [Email.view (Signal.forwardTo address UpdateEmail) email]
        Reminder reminder -> Html.p [getItemStyle item styling] [Reminder.view (Signal.forwardTo address UpdateReminder) currentDate reminder]