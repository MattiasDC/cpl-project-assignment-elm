module EmailFetcherHelper where

import Http
import Json.Decode as Json exposing ((:=))
import Task exposing (..)
import Signal
import List
import ItemFeed
import Item

-- Module for helping methods for fetching emails

type alias InitEmailParameters = 
    { 
    from : String
    , to : String
    , title : String
    , body : String
    , date : String
    }


-- Send to the mailbox of the given address an action to add the emails in the given list that contain email parameters.
extractTask : Signal.Address (Maybe ItemFeed.Action) -> List InitEmailParameters -> Task x ()
extractTask address listOfEmails =
   Signal.send address (Just <| ItemFeed.AddMultiple <| List.map (\m -> Item.initEmail m) listOfEmails)

-- Send an http process to the page contains json email parameters
-- and forward it for extraction of emails and sending updates to the itemfeed
processTask : Signal.Address (Maybe ItemFeed.Action) -> Task x ()
processTask address =
    Http.get emails "https://api.myjson.com/bins/19lg3" `onError` (\e -> succeed []) `andThen` extractTask address

emails : Json.Decoder (List InitEmailParameters)
emails =
  let email =
        Json.object5 (\f t ti d b -> { from = f, to = t, title = ti, body = b, date = d})
          ("from" := Json.string)
          ("to" := Json.string)
          ("title" := Json.string)
          ("date" := Json.string)
          ("body" := Json.string)
  in
      "emails" := Json.list email