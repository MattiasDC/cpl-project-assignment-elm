module Reminder where

import Html exposing ( Html )
import Html.Events as E
import Html.Attributes exposing ( style, type', name, value )
import String exposing ( concat )
import DateManager
import Date exposing ( Date )

-- Module representing a reminder with an optional deadline

type alias Model =
  { body : String
  , created : Date
  , done : Bool
  , pinned : Bool
  , deadline : Maybe Date
  , snoozed : Maybe Date
  , snoozeInputField : String }

init : String -> Date -> Maybe Date -> Model
init b d dl = 
    { body = b
    , created = d
    , done = False
    , pinned = False
    , deadline = dl
    , snoozed = Nothing
    , snoozeInputField = DateManager.dateToString DateManager.beginDate }

equal : Model -> Model -> Bool
equal model1 model2 =
    if model1.body == model2.body && model1.created == model2.created then
        True 
    else
        False
type Action = Mark
            | Pin
            | Snooze Date
            | UpdateSnoozeField String


update : Action -> Model -> Model
update action model =
    case action of
        Mark                -> { model - done | done = not model.done }
        Pin                 -> { model - pinned | pinned = not model.pinned }
        Snooze d            -> { model - snoozed | snoozed = (Just d) }
        UpdateSnoozeField d -> { model - snoozeInputField | snoozeInputField = d }

getDeadlineHtml : Model -> Maybe Html
getDeadlineHtml model =
    case model.deadline of
        Just deadline -> Just <| Html.text <| concat [ "deadline: ", DateManager.dateToString deadline]
        Nothing -> Nothing

extractHtml : Maybe Html -> Html
extractHtml m =
    case m of
        Just h -> h
        Nothing -> Html.text ""

extractHtmlAttributes : Maybe Html.Attribute -> Html.Attribute
extractHtmlAttributes m =
    Maybe.withDefault (style []) m

-- Styling for deadlines, if the item is not done and the current date is past the deadline, a red border is shown
getDeadlineStyle : Model -> Date -> Maybe Html.Attribute
getDeadlineStyle model currentdate =
    case model.deadline of
        Just deadline ->    if DateManager.compareDate currentdate deadline == GT && not model.done then
                                Just <| style [("border-width", "thick"),
                                    ("border-style", "solid"),
                                    ("border-color", "Red"),
                                    ("padding", "5px 5px 10px")]
                            else
                                Nothing
        Nothing -> Nothing

view : Signal.Address Action -> Date -> Model -> Html
view address currentDate model =
    Html.div [extractHtmlAttributes <| getDeadlineStyle model currentDate] <|
        Html.text model.body::
        [Html.p []
            [
                Html.button
                    [ E.onClick address Mark ]
                    [ Html.text <| if model.done then "Undo" else "Mark as done" ]
                , Html.button
                    [ E.onClick address Pin ]
                    [ Html.text <| if model.pinned then "Unpin" else "Pin" ]
                , Html.button
                    [ E.onClick address (Snooze (DateManager.fromString model.snoozeInputField))]
                    [ Html.text "Snooze" ]
                -- On input in the date text field for snoozing, the value of the inputfield in the model is updated
                -- This value is used when the snooze button is actually pressed
                , Html.input [type' "date", name "snoozeDate", value model.snoozeInputField,
                                                E.on "input" E.targetValue
                                                    (\str -> Signal.message address
                                                        (UpdateSnoozeField str))]
                                            [Html.text model.snoozeInputField]
            ]
        , Html.p [] [Html.text <| concat [ "date: ", DateManager.dateToString model.created]
                    , Html.br [] []
                    , extractHtml <| getDeadlineHtml model
                    ]
        ]