module Email where

import Html exposing ( Html )
import Html.Events as E
import Html.Attributes exposing ( type', name, value )
import String exposing ( concat )
import DateManager
import Date exposing ( Date )

-- Module representing an email

short : Int
short = 200

type alias Model =
  { from : String
  , to : String
  , title : String
  , body : String
  , date : Date
  , showMore : Bool
  , done : Bool
  , pinned : Bool
  , snoozed : Maybe Date
  , snoozeInputField : String
  }

init : String -> String -> String -> String -> Date -> Model
init f t ti b d = 
    { from = f
    , to = t
    , title = ti
    , body = b
    , date = d
    , showMore = False
    , done = False
    , pinned = False
    , snoozed = Nothing
    , snoozeInputField = DateManager.dateToString DateManager.beginDate }

equal : Model -> Model -> Bool
equal model1 model2 =
    if model1.from == model2.from && model1.to == model2.to
        && model1.title == model2.title && model1.body == model2.body
        && DateManager.equal model1.date model2.date then
        True 
    else
        False

type Action = Mark
            | Pin
            | More
            | Snooze Date
            | UpdateSnoozeField String


update : Action -> Model -> Model
update action model =
    case action of
        Mark                  -> { model - done | done = not model.done }
        Pin                   -> { model - pinned | pinned = not model.pinned }
        More                  -> { model - showMore | showMore = not model.showMore }
        Snooze d              -> { model - snoozed | snoozed = (Just d) }
        UpdateSnoozeField d   -> { model - snoozeInputField | snoozeInputField = d}

isShort : Model -> Bool
isShort model =
  String.length model.body <= short

-- Method for showing the actual message shown on the page,
--based on the length of the message and whether it is truncated or not
getMessage : Model -> String
getMessage model =  if isShort model then
                        model.body
                      else
                        if model.showMore then
                          model.body
                        else
                          String.append (String.left short model.body) "..."

view : Signal.Address Action -> Model -> Html
view address model =
    Html.div [] <|
        List.append
        [
             Html.text (concat [ model.title, " | ", model.from, "says:" ])
           , Html.text <| getMessage model
        ]
        [Html.p [] <|
                  (::) (if not <| isShort model then 
                    Html.button
                      [ E.onClick address More ]
                      [ Html.text <| if model.showMore then "Less" else "More" ]
                  else
                    Html.text "")
              [
                  Html.button
                      [ E.onClick address Mark ]
                      [ Html.text <| if model.done then "Undo" else "Mark as done" ]
                  , Html.button
                      [ E.onClick address Pin ]
                      [ Html.text <| if model.pinned then "Unpin" else "Pin" ]
                  , Html.button
                    [ E.onClick address (Snooze <| DateManager.fromString model.snoozeInputField)]
                    [ Html.text "Snooze" ]
                  -- On input in the date text field for snoozing, the value of the inputfield in the model is updated
                  -- This value is used when the snooze button is actually pressed
                  , Html.input [type' "date", name "snoozeDate", value model.snoozeInputField,
                                                E.on "input" E.targetValue
                                                    (\str -> Signal.message address
                                                        (UpdateSnoozeField str))]
                                            [Html.text model.snoozeInputField]
              ]
          , Html.p [] [Html.text <| concat [ "date: ", DateManager.dateToString model.date ]]
        ]