module DateManager where
import Time
import Date exposing ( Date, Month )
import String

-- Module with methods to work with dates since the Date module of elm cannot compare
-- or transform a date into a string representation

equal : Date -> Date -> Bool
equal d1 d2 =
    (dateToString d1) == (dateToString d2)

compareDate : Date -> Date -> Order
compareDate d1 d2 =
    compare (dateToString d1) (dateToString d2)

dateToString : Date -> String
dateToString date =
    String.concat [toString <| Date.year date, "-", monthToIntString <| Date.month date, "-", toStringInt <| Date.day date]

fromString : String -> Date
fromString d =
    Maybe.withDefault (Date.fromTime Time.second) <| Result.toMaybe <| Date.fromString d

monthToIntString : Month -> String
monthToIntString month = 
    case month of 
        Date.Jan -> "01"
        Date.Feb -> "02"
        Date.Mar -> "03"
        Date.Apr -> "04"
        Date.May -> "05"
        Date.Jun -> "06"
        Date.Jul -> "07"
        Date.Aug -> "08"
        Date.Sep -> "09"
        Date.Oct -> "10"
        Date.Nov -> "11"
        Date.Dec -> "12"

toStringInt : Int -> String
toStringInt d =
    if String.length (toString d) >= 2 then
        toString d 
    else
        String.append "0" (toString d)

beginDate : Date 
beginDate = Date.fromTime Time.second