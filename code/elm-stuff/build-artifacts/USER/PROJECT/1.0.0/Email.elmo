Elm.Email = Elm.Email || {};
Elm.Email.make = function (_elm) {
   "use strict";
   _elm.Email = _elm.Email || {};
   if (_elm.Email.values)
   return _elm.Email.values;
   var _op = {},
   _N = Elm.Native,
   _U = _N.Utils.make(_elm),
   _L = _N.List.make(_elm),
   $moduleName = "Email",
   $Basics = Elm.Basics.make(_elm),
   $Date = Elm.Date.make(_elm),
   $DateManager = Elm.DateManager.make(_elm),
   $Html = Elm.Html.make(_elm),
   $Html$Attributes = Elm.Html.Attributes.make(_elm),
   $Html$Events = Elm.Html.Events.make(_elm),
   $List = Elm.List.make(_elm),
   $Maybe = Elm.Maybe.make(_elm),
   $Result = Elm.Result.make(_elm),
   $Signal = Elm.Signal.make(_elm),
   $String = Elm.String.make(_elm);
   var update = F2(function (action,
   model) {
      return function () {
         switch (action.ctor)
         {case "Mark":
            return _U.insert("done",
              $Basics.not(model.done),
              _U.remove("done",model));
            case "More":
            return _U.insert("showMore",
              $Basics.not(model.showMore),
              _U.remove("showMore",model));
            case "Pin":
            return _U.insert("pinned",
              $Basics.not(model.pinned),
              _U.remove("pinned",model));
            case "Snooze":
            return _U.insert("snoozed",
              $Maybe.Just(action._0),
              _U.remove("snoozed",model));
            case "UpdateSnoozeField":
            return _U.insert("snoozeInputField",
              action._0,
              _U.remove("snoozeInputField",
              model));}
         _U.badCase($moduleName,
         "between lines 59 and 64");
      }();
   });
   var UpdateSnoozeField = function (a) {
      return {ctor: "UpdateSnoozeField"
             ,_0: a};
   };
   var Snooze = function (a) {
      return {ctor: "Snooze"
             ,_0: a};
   };
   var More = {ctor: "More"};
   var Pin = {ctor: "Pin"};
   var Mark = {ctor: "Mark"};
   var equal = F2(function (model1,
   model2) {
      return _U.eq(model1.from,
      model2.from) && (_U.eq(model1.to,
      model2.to) && (_U.eq(model1.title,
      model2.title) && (_U.eq(model1.body,
      model2.body) && A2($DateManager.equal,
      model1.date,
      model2.date)))) ? true : false;
   });
   var init = F5(function (f,
   t,
   ti,
   b,
   d) {
      return {_: {}
             ,body: b
             ,date: d
             ,done: false
             ,from: f
             ,pinned: false
             ,showMore: false
             ,snoozeInputField: $DateManager.dateToString($DateManager.beginDate)
             ,snoozed: $Maybe.Nothing
             ,title: ti
             ,to: t};
   });
   var Model = function (a) {
      return function (b) {
         return function (c) {
            return function (d) {
               return function (e) {
                  return function (f) {
                     return function (g) {
                        return function (h) {
                           return function (i) {
                              return function (j) {
                                 return {_: {}
                                        ,body: d
                                        ,date: e
                                        ,done: g
                                        ,from: a
                                        ,pinned: h
                                        ,showMore: f
                                        ,snoozeInputField: j
                                        ,snoozed: i
                                        ,title: c
                                        ,to: b};
                              };
                           };
                        };
                     };
                  };
               };
            };
         };
      };
   };
   var $short = 200;
   var isShort = function (model) {
      return _U.cmp($String.length(model.body),
      $short) < 1;
   };
   var getMessage = function (model) {
      return isShort(model) ? model.body : model.showMore ? model.body : A2($String.append,
      A2($String.left,
      $short,
      model.body),
      "...");
   };
   var view = F2(function (address,
   model) {
      return $Html.div(_L.fromArray([]))(A2($List.append,
      _L.fromArray([$Html.text($String.concat(_L.fromArray([model.title
                                                           ," | "
                                                           ,model.from
                                                           ,"says:"])))
                   ,$Html.text(getMessage(model))]),
      _L.fromArray([$Html.p(_L.fromArray([]))(A2(F2(function (x,
                   y) {
                      return A2($List._op["::"],
                      x,
                      y);
                   }),
                   $Basics.not(isShort(model)) ? A2($Html.button,
                   _L.fromArray([A2($Html$Events.onClick,
                   address,
                   More)]),
                   _L.fromArray([$Html.text(model.showMore ? "Less" : "More")])) : $Html.text(""),
                   _L.fromArray([A2($Html.button,
                                _L.fromArray([A2($Html$Events.onClick,
                                address,
                                Mark)]),
                                _L.fromArray([$Html.text(model.done ? "Undo" : "Mark as done")]))
                                ,A2($Html.button,
                                _L.fromArray([A2($Html$Events.onClick,
                                address,
                                Pin)]),
                                _L.fromArray([$Html.text(model.pinned ? "Unpin" : "Pin")]))
                                ,A2($Html.button,
                                _L.fromArray([A2($Html$Events.onClick,
                                address,
                                Snooze($DateManager.fromString(model.snoozeInputField)))]),
                                _L.fromArray([$Html.text("Snooze")]))
                                ,A2($Html.input,
                                _L.fromArray([$Html$Attributes.type$("date")
                                             ,$Html$Attributes.name("snoozeDate")
                                             ,$Html$Attributes.value(model.snoozeInputField)
                                             ,A3($Html$Events.on,
                                             "input",
                                             $Html$Events.targetValue,
                                             function (str) {
                                                return A2($Signal.message,
                                                address,
                                                UpdateSnoozeField(str));
                                             })]),
                                _L.fromArray([$Html.text(model.snoozeInputField)]))])))
                   ,A2($Html.p,
                   _L.fromArray([]),
                   _L.fromArray([$Html.text($String.concat(_L.fromArray(["date: "
                                                                        ,$DateManager.dateToString(model.date)])))]))])));
   });
   _elm.Email.values = {_op: _op
                       ,$short: $short
                       ,Model: Model
                       ,init: init
                       ,equal: equal
                       ,Mark: Mark
                       ,Pin: Pin
                       ,More: More
                       ,Snooze: Snooze
                       ,UpdateSnoozeField: UpdateSnoozeField
                       ,update: update
                       ,isShort: isShort
                       ,getMessage: getMessage
                       ,view: view};
   return _elm.Email.values;
};