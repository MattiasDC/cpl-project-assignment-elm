module Main where

import Html exposing ( Html )
import Signal
import Date
import Time
import Item
import Reminder
import ItemFeed
import HotKeysManager
import EmailFetcherHelper
import Http
import Task exposing (..)

-- Name: Mattias De Charleroy
-- Student ID: r0304492


-- * Add a hotkey to toggle the visibility of 'done' items.
-- Status: Completed
-- Summary: Alt-H to toggle the visibility,
--          an extra shortkey in the hotKeysManager + extra action in the itemfeed was necessary for this.
--          So was a boolean value in the model of itemFeed that indicates whether the done section should be shown or not.


-- * Hide the 'add reminder' functionality and add a hotkey to toggle its
-- * visibility.
-- Status: Completed
-- Summary: Alt-A to toggle the visibility,
--          an extra shortkey in the hotKeysManager + extra action in the itemfeed was necessary for this.
--          So was a boolean value in the model of itemFeed that indicates whether the add reminder section should be shown or not.


-- * Put the current date as the default in the date picker when adding
-- * reminders.
-- Status: Completed
-- Summary: In the add reminder section the input field is standard on the current date.
--          So is the optional deadline field (see next extension). In the main module a signal
--          is added that updates the current date in the itemFeed every second. This currentDate values
--          is used to initialize field, check for reminder deadline.


-- * Add a deadline property to reminders and mark all reminders that are past
-- * their deadline.
-- Status: Completed
-- Summary: Extra input field to adjust deadline date. Extra button to use this deadline.
--          No extra reminder module is added, a Maybe Date attribute is added to the reminder module.
--          The currentDate of the itemFeed model is passed to the view of item and reminder to have other styling possibility.


-- * Add a 'snooze' feature to items, to 'snooze' an item you must provide a
-- * date on which the item has to 'un-snooze'. 'snoozed' items are not visible.
-- Status: Completed
-- Summary: Extra button besides the pin button. Item will show up again after the given date.
--          Snooze dates that are already passed have thus no effect. (except on the selected focus)
--          These are extra actions in Reminder and Email. Item provides a method that returns a boolean
--          value that indicates whether the item is snoozed or not, this method is used in itemFeed to properly not show snoozed items.

-- * On startup, read e-mails from a Json document at this url:
-- * http://people.cs.kuleuven.be/~bob.reynders/2015-2016/emails.json
-- Status: Completed
-- Summary: See next extension


-- * Periodically check for e-mails from Json (same url).
-- Status: Completed
-- Summary: Every minute a http request is sent to an url provided in the discussion board
--          to bypass the same origin policy. ItemFeed has an extra action 'AddMultiple',
--          This call the 'Add item' action for each element provided in the list using a fold to update the model.
--          A check is placed in the 'Add item' to ignore duplicate items.
--          The only adjustments were:  -An extra action in ItemFeed + method to check for duplicates in model.
--                                      -Module EmailFetcherHelper
--                                      -Main method providing a port. 


-- * Add persistence to your application by using Html local storage so that
-- * newly added reminders are still there after a reload.
-- Status: Unattempted
-- Summary: 


-- * Come up with your own extension!
-- Status: Unattempted
-- Summary: 


-- Start of program

mailbox : Signal.Mailbox (Maybe ItemFeed.Action)
mailbox = Signal.mailbox Nothing

state : Signal ItemFeed.Model
state =
  let update action model =
        case action of
          Just a -> ItemFeed.update a model
          _ -> model
  in Signal.foldp update ItemFeed.init
                    -- Merge the signals of the different signal sources: mailbox, keyboard, date (current time).
                   <| Signal.mergeMany [HotKeysManager.signal, mailbox.signal, dateSignal]

-- Signal the is produced every second to update the time in the model
dateSignal : Signal (Maybe ItemFeed.Action)
dateSignal =
    Signal.map (\t -> Just <| ItemFeed.UpdateDateTime (Date.fromTime t)) (Time.every Time.second)

-- Port for checking new mails every minute
port fetchMails : Signal (Task x ())
port fetchMails =
    Signal.map (\s -> EmailFetcherHelper.processTask mailbox.address) (Time.every Time.minute)


main : Signal Html
main = 
    let view = ItemFeed.view (Signal.forwardTo mailbox.address Just)
    in Signal.map view state